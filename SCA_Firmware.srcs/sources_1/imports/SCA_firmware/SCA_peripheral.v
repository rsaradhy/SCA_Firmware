`timescale 1ns / 1ps

module SCA_peripheral
  (input reset,
   input clk40, clk80, clk160,

   input [15:0] firmware_version,
   input [9:0] peripheral_addr,
   input peripheral_write,
   input [15:0] data_write,
   input peripheral_read,
   output [15:0] data_read,
   
   input fmc_sw3,
   input fmc_sw4,

   output reg fmc_led3,
   output reg fmc_led4,
   output reg sca_resetb,
   output reg aux_link_disable,
   
   output reg sca_gpio0,
   output reg sca_gpio1,
   output reg sca_gpio2,
   
   input sca_gpio8,
   input sca_gpio9,
   input sca_gpio10,
   input sca_gpio11,
   input sca_gpio12,

   output fmc_mosi_p, fmc_mosi_n,
   output fmc_clk_p, fmc_clk_n, 
   output fmc_usr_p, fmc_usr_n, 
   input fmc_miso_p, fmc_miso_n);
   
   //----------------------------------------------------------------------
   // Define the spi peripheral addresses.
   //----------------------------------------------------------------------
   `include "SCA_ADDR.v"
   //----------------------------------------------------------------------

   //----------------------------------------------------------------------
   // Declare the spi peripheral signals.
   //----------------------------------------------------------------------

   reg [15:0] dummy_reg0, dummy_reg1;
   wire [15:0] constant0, constant1;
   assign constant0 = 16'hDEAD;
   assign constant1 = 16'hBEEF;

   reg elink_enable;
   reg send_packet1;
   reg send_packet2;
   reg send_packet3;
   
   reg [15:0] bits_packet0;
   reg [15:0] bits_packet1;
   reg [15:0] bits_packet2;
   reg [15:0] bits_packet3;

   reg [15:0] tx_packet0[7:0]; // 128 bits available for tx packet0
   //reg [15:0] rx_packet0[7:0]; // 128 bits available for rx packet0
   wire [15:0] rx_packet0[7:0]; // 128 bits available for rx packet0

   reg [15:0] tx_packet1[7:0]; // 128 bits available for tx packet1
   //reg [15:0] rx_packet1[7:0]; // 128 bits available for rx packet1
   wire [15:0] rx_packet1[7:0]; // 128 bits available for rx packet1

   reg [15:0] tx_packet2[7:0]; // 128 bits available for tx packet2
   //reg [15:0] rx_packet2[7:0]; // 128 bits available for rx packet2
   wire [15:0] rx_packet2[7:0]; // 128 bits available for rx packet2

   reg [15:0] tx_packet3[7:0]; // 128 bits available for tx packet3   
   //reg [15:0] rx_packet3[7:0]; // 128 bits available for rx packet3
   wire [15:0] rx_packet3[7:0]; // 128 bits available for rx packet3

   //----------------------------------------------------------------------
   // Declare misc. signals.
   //----------------------------------------------------------------------

   reg fmc_mosi, fmc_usr;
   wire fmc_miso;
   reg sent0, sent1, sent2, sent3;
   reg [127:0] shift_mosi;
   reg [127:0] shift_miso,shift_miso0,shift_miso1,shift_miso2,shift_miso3;
   reg [7:0] shift_counter;

   wire [7:0] IDLE_CMD, FRAME_CMD;
   assign IDLE_CMD  = 8'b11111110;
   assign FRAME_CMD = 8'b01111110;

   //----------------------------------------------------------------------
   // Pulse the peripheral controls for one tick.
   //----------------------------------------------------------------------
   
   reg write_reg1, write_reg2, write_pulse;
   always @(posedge clk80) write_reg1 <= peripheral_write;
   always @(posedge clk80) write_reg2 <= write_reg1;
   always @(posedge clk80) write_pulse <= write_reg1 & ~write_reg2;
   
   reg read_reg1, read_reg2, read_pulse;
   always @(posedge clk80) read_reg1 <= peripheral_read;
   always @(posedge clk80) read_reg2 <= read_reg1;
   always @(posedge clk80) read_pulse <= read_reg1 & ~read_reg2;

   //----------------------------------------------------------------------
   // Clean up the synthesis timing for 80 MHz.
   //----------------------------------------------------------------------
   
   reg [9:0] peripheral_addr_q; // SPI address
   always @(posedge clk80) peripheral_addr_q <= peripheral_addr;
   
   reg [15:0] data_write_q; // SPI data
   always @(posedge clk80) data_write_q <= data_write;
   
   //----------------------------------------------------------------------
   // Compute data_read.
   //----------------------------------------------------------------------

   wire [2:0] indx;
   assign indx = peripheral_addr_q[2:0];

   wire [9:0] addr_mask;
   assign addr_mask = 10'h3F8;
   
   assign data_read 
     = ((peripheral_addr_q == SCA_FIRMWARE_VERSION) ? firmware_version:16'h0) |
       ((peripheral_addr_q == SCA_CONSTANT0) ? constant0 : 16'h0) |
       ((peripheral_addr_q == SCA_CONSTANT1) ? constant1 : 16'h0) |
       ((peripheral_addr_q == SCA_DUMMY_REG0) ? dummy_reg0 : 16'h0) |
       ((peripheral_addr_q == SCA_DUMMY_REG1) ? dummy_reg1 : 16'h0) |

       ((peripheral_addr_q == SCA_GPIO8) ? {15'h0, sca_gpio8} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO9) ? {15'h0, sca_gpio9} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO10) ? {15'h0, sca_gpio10} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO11) ? {15'h0, sca_gpio11} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO12) ? {15'h0, sca_gpio12} : 16'h0) |
       ((peripheral_addr_q == SCA_SW3) ? {15'h0, fmc_sw3} : 16'h0) |
       ((peripheral_addr_q == SCA_SW4) ? {15'h0, fmc_sw4} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO0) ? {15'h0, sca_gpio0} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO1) ? {15'h0, sca_gpio1} : 16'h0) |
       ((peripheral_addr_q == SCA_GPIO2) ? {15'h0, sca_gpio2} : 16'h0) |
       ((peripheral_addr_q == SCA_LED3) ? {15'h0, fmc_led3} : 16'h0) |
       ((peripheral_addr_q == SCA_LED4) ? {15'h0, fmc_led4} : 16'h0) |
       ((peripheral_addr_q == SCA_AUX_DISABLE) ? {15'h0, aux_link_disable} : 16'h0) |
       ((peripheral_addr_q == SCA_RESETB) ? {15'h0, sca_resetb} : 16'h0) |
       ((peripheral_addr_q == SCA_ELINK_ENABLE) ? {15'h0, elink_enable} : 16'h0) |
       
       ((peripheral_addr_q == SCA_BITS_PACKET0) ? bits_packet0 : 16'h0) |
       ((peripheral_addr_q == SCA_BITS_PACKET1) ? bits_packet1 : 16'h0) |
       ((peripheral_addr_q == SCA_BITS_PACKET2) ? bits_packet2 : 16'h0) |
       ((peripheral_addr_q == SCA_BITS_PACKET3) ? bits_packet3 : 16'h0) |
       
       (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET0) ? tx_packet0[indx] : 16'h0) |
       (((peripheral_addr_q & addr_mask) == SCA_RX_PACKET0) ? rx_packet0[indx] : 16'h0) |

       (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET1) ? tx_packet1[indx] : 16'h0) |
       (((peripheral_addr_q & addr_mask) == SCA_RX_PACKET1) ? rx_packet1[indx] : 16'h0) |

       (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET2) ? tx_packet2[indx] : 16'h0) |
       (((peripheral_addr_q & addr_mask) == SCA_RX_PACKET2) ? rx_packet2[indx] : 16'h0) |

       (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET3) ? tx_packet3[indx] : 16'h0) |
       (((peripheral_addr_q & addr_mask) == SCA_RX_PACKET3) ? rx_packet3[indx] : 16'h0);

   //----------------------------------------------------------------------
   // Capture data_write.
   //----------------------------------------------------------------------
   
   always @(posedge clk80) // dummy_reg0
     if (reset == 1'b1) dummy_reg0 <= 16'h0;
     else if ((peripheral_addr_q == SCA_DUMMY_REG0) && 
	      (write_pulse == 1'b1)) dummy_reg0 <= data_write;
     else dummy_reg0 <= dummy_reg0;
   
   always @(posedge clk80) // dummy_reg1
     if (reset == 1'b1) dummy_reg1 <= 16'h0;     
     else if ((peripheral_addr_q == SCA_DUMMY_REG1) && 
	      (write_pulse == 1'b1)) dummy_reg1 <= data_write;
     else dummy_reg1 <= dummy_reg1;

   always @(posedge clk80) // sca_gpio0
     if (reset == 1'b1) sca_gpio0 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_GPIO0) && 
	      (write_pulse == 1'b1)) sca_gpio0 <= data_write[0];
     else sca_gpio0 <= sca_gpio0;

   always @(posedge clk80) // sca_gpio1
     if (reset == 1'b1) sca_gpio1 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_GPIO1) && 
	      (write_pulse == 1'b1)) sca_gpio1 <= data_write[0];
     else sca_gpio1 <= sca_gpio1;

   always @(posedge clk80) // sca_gpio2
     if (reset == 1'b1) sca_gpio2 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_GPIO2) && 
	      (write_pulse == 1'b1)) sca_gpio2 <= data_write[0];
     else sca_gpio2 <= sca_gpio2;

   always @(posedge clk80) // fmc_led3
     if (reset == 1'b1) fmc_led3 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_LED3) && 
	      (write_pulse == 1'b1)) fmc_led3 <= data_write[0];
     else fmc_led3 <= fmc_led3;

   always @(posedge clk80) // fmc_led4
     if (reset == 1'b1) fmc_led4 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_LED4) && 
	      (write_pulse == 1'b1)) fmc_led4 <= data_write[0];
     else fmc_led4 <= fmc_led4;

   always @(posedge clk80) // aux_link_disable
     if (reset == 1'b1) aux_link_disable <= 1'b1; // !! default is disabled !!
     else if ((peripheral_addr_q == SCA_AUX_DISABLE) && 
	      (write_pulse == 1'b1)) aux_link_disable <= data_write[0];
     else aux_link_disable <= aux_link_disable;
   
   always @(posedge clk80) // sca_resetb
     if (reset == 1'b1) sca_resetb <= 1'b0;     
     else if ((peripheral_addr_q == SCA_RESETB) && 
	      (write_pulse == 1'b1)) sca_resetb <= data_write[0];
     else sca_resetb <= sca_resetb;

   always @(posedge clk80) // elink_enable
     if (reset == 1'b1) elink_enable <= 1'b1; // initialize to 1 at reset
     else if ((peripheral_addr_q == SCA_ELINK_ENABLE) && 
	      (write_pulse == 1'b1)) elink_enable <= data_write[0];
     else elink_enable <= elink_enable;
   //elink_enable <= 1'b1; // Manually setting Elink_Enable to 1
     
   always @(posedge clk80) // send_packet1
     if (reset == 1'b1) send_packet1 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_SEND_PACKET1) && (write_pulse == 1'b1)) 
       send_packet1 <= elink_enable & data_write[0];
     else send_packet1 <= ~sent1 & send_packet1;
   
   always @(posedge clk80) // send_packet2
     if (reset == 1'b1) send_packet2 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_SEND_PACKET2) && (write_pulse == 1'b1)) 
       send_packet2 <= elink_enable & data_write[0];
     else send_packet2 <= ~sent2 & send_packet2;
   
   always @(posedge clk80) // send_packet3
     if (reset == 1'b1) send_packet3 <= 1'b0;     
     else if ((peripheral_addr_q == SCA_SEND_PACKET3) && (write_pulse == 1'b1)) 
       send_packet3 <= elink_enable & data_write[0];
     else send_packet3 <= ~sent3 & send_packet3;
   
   always @(posedge clk80) // bits_packet0
     if (reset == 1'b1) bits_packet0 <= 16'hF;     
     else if ((peripheral_addr_q == SCA_BITS_PACKET0) && 
	      (write_pulse == 1'b1)) bits_packet0 <= data_write;
     else bits_packet0 <= bits_packet0;

   always @(posedge clk80) // bits_packet1
     if (reset == 1'b1) bits_packet1 <= 16'hF;     
     else if ((peripheral_addr_q == SCA_BITS_PACKET1) && 
	      (write_pulse == 1'b1)) bits_packet1 <= data_write;
     else bits_packet1 <= bits_packet1;

   always @(posedge clk80) // bits_packet2
     if (reset == 1'b1) bits_packet2 <= 16'hF;     
     else if ((peripheral_addr_q == SCA_BITS_PACKET2) && 
	      (write_pulse == 1'b1)) bits_packet2 <= data_write;
     else bits_packet2 <= bits_packet2;

   always @(posedge clk80) // bits_packet3
     if (reset == 1'b1) bits_packet3 <= 16'hF;     
     else if ((peripheral_addr_q == SCA_BITS_PACKET3) && 
	      (write_pulse == 1'b1)) bits_packet3 <= data_write;
     else bits_packet3 <= bits_packet3;
   
   always @(posedge clk80) // tx_packet0
     if (reset == 1'b1) begin
	tx_packet0[0] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[1] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[2] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[3] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[4] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[5] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[6] <= {IDLE_CMD, IDLE_CMD};
	tx_packet0[7] <= {IDLE_CMD, IDLE_CMD};
     end
     else if (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET0) && 
	      (write_pulse == 1'b1)) begin
	if (indx == 3'h0) tx_packet0[0] <= data_write;
	else tx_packet0[0] <= tx_packet0[0];
	if (indx == 3'h1) tx_packet0[1] <= data_write;
	else tx_packet0[1] <= tx_packet0[1];
	if (indx == 3'h2) tx_packet0[2] <= data_write;
	else tx_packet0[2] <= tx_packet0[2];
	if (indx == 3'h3) tx_packet0[3] <= data_write;
	else tx_packet0[3] <= tx_packet0[3];
	if (indx == 3'h4) tx_packet0[4] <= data_write;
	else tx_packet0[4] <= tx_packet0[4];
	if (indx == 3'h5) tx_packet0[5] <= data_write;
	else tx_packet0[5] <= tx_packet0[5];
	if (indx == 3'h6) tx_packet0[6] <= data_write;
	else tx_packet0[6] <= tx_packet0[6];
	if (indx == 3'h7) tx_packet0[7] <= data_write;
	else tx_packet0[7] <= tx_packet0[7];
     end
     else begin
	tx_packet0[0] <= tx_packet0[0];
	tx_packet0[1] <= tx_packet0[1];
	tx_packet0[2] <= tx_packet0[2];
	tx_packet0[3] <= tx_packet0[3];
	tx_packet0[4] <= tx_packet0[4];
	tx_packet0[5] <= tx_packet0[5];
	tx_packet0[6] <= tx_packet0[6];
	tx_packet0[7] <= tx_packet0[7];
     end

   always @(posedge clk80) // tx_packet1
     if (reset == 1'b1) begin
	tx_packet1[0] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[1] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[2] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[3] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[4] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[5] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[6] <= {IDLE_CMD, IDLE_CMD};
	tx_packet1[7] <= {IDLE_CMD, IDLE_CMD};
     end
     else if (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET1) && 
	      (write_pulse == 1'b1)) begin
	if (indx == 3'h0) tx_packet1[0] <= data_write;
	else tx_packet1[0] <= tx_packet1[0];
	if (indx == 3'h1) tx_packet1[1] <= data_write;
	else tx_packet1[1] <= tx_packet1[1];
	if (indx == 3'h2) tx_packet1[2] <= data_write;
	else tx_packet1[2] <= tx_packet1[2];
	if (indx == 3'h3) tx_packet1[3] <= data_write;
	else tx_packet1[3] <= tx_packet1[3];
	if (indx == 3'h4) tx_packet1[4] <= data_write;
	else tx_packet1[4] <= tx_packet1[4];
	if (indx == 3'h5) tx_packet1[5] <= data_write;
	else tx_packet1[5] <= tx_packet1[5];
	if (indx == 3'h6) tx_packet1[6] <= data_write;
	else tx_packet1[6] <= tx_packet1[6];
	if (indx == 3'h7) tx_packet1[7] <= data_write;
	else tx_packet1[7] <= tx_packet1[7];
     end
     else begin
	tx_packet1[0] <= tx_packet1[0];
	tx_packet1[1] <= tx_packet1[1];
	tx_packet1[2] <= tx_packet1[2];
	tx_packet1[3] <= tx_packet1[3];
	tx_packet1[4] <= tx_packet1[4];
	tx_packet1[5] <= tx_packet1[5];
	tx_packet1[6] <= tx_packet1[6];
	tx_packet1[7] <= tx_packet1[7];
     end

   always @(posedge clk80) // tx_packet2
     if (reset == 1'b1) begin
	tx_packet2[0] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[1] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[2] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[3] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[4] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[5] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[6] <= {IDLE_CMD, IDLE_CMD};
	tx_packet2[7] <= {IDLE_CMD, IDLE_CMD};
     end
     else if (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET2) && 
	      (write_pulse == 1'b1)) begin
	if (indx == 3'h0) tx_packet2[0] <= data_write;
	else tx_packet2[0] <= tx_packet2[0];
	if (indx == 3'h1) tx_packet2[1] <= data_write;
	else tx_packet2[1] <= tx_packet2[1];
	if (indx == 3'h2) tx_packet2[2] <= data_write;
	else tx_packet2[2] <= tx_packet2[2];
	if (indx == 3'h3) tx_packet2[3] <= data_write;
	else tx_packet2[3] <= tx_packet2[3];
	if (indx == 3'h4) tx_packet2[4] <= data_write;
	else tx_packet2[4] <= tx_packet2[4];
	if (indx == 3'h5) tx_packet2[5] <= data_write;
	else tx_packet2[5] <= tx_packet2[5];
	if (indx == 3'h6) tx_packet2[6] <= data_write;
	else tx_packet2[6] <= tx_packet2[6];
	if (indx == 3'h7) tx_packet2[7] <= data_write;
	else tx_packet2[7] <= tx_packet2[7];
     end
     else begin
	tx_packet2[0] <= tx_packet2[0];
	tx_packet2[1] <= tx_packet2[1];
	tx_packet2[2] <= tx_packet2[2];
	tx_packet2[3] <= tx_packet2[3];
	tx_packet2[4] <= tx_packet2[4];
	tx_packet2[5] <= tx_packet2[5];
	tx_packet2[6] <= tx_packet2[6];
	tx_packet2[7] <= tx_packet2[7];
     end

   always @(posedge clk80) // tx_packet3
     if (reset == 1'b1) begin
	tx_packet3[0] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[1] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[2] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[3] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[4] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[5] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[6] <= {IDLE_CMD, IDLE_CMD};
	tx_packet3[7] <= {IDLE_CMD, IDLE_CMD};
     end
     else if (((peripheral_addr_q & addr_mask) == SCA_TX_PACKET3) && 
	      (write_pulse == 1'b1)) begin
	if (indx == 3'h0) tx_packet3[0] <= data_write;
	else tx_packet3[0] <= tx_packet3[0];
	if (indx == 3'h1) tx_packet3[1] <= data_write;
	else tx_packet3[1] <= tx_packet3[1];
	if (indx == 3'h2) tx_packet3[2] <= data_write;
	else tx_packet3[2] <= tx_packet3[2];
	if (indx == 3'h3) tx_packet3[3] <= data_write;
	else tx_packet3[3] <= tx_packet3[3];
	if (indx == 3'h4) tx_packet3[4] <= data_write;
	else tx_packet3[4] <= tx_packet3[4];
	if (indx == 3'h5) tx_packet3[5] <= data_write;
	else tx_packet3[5] <= tx_packet3[5];
	if (indx == 3'h6) tx_packet3[6] <= data_write;
	else tx_packet3[6] <= tx_packet3[6];
	if (indx == 3'h7) tx_packet3[7] <= data_write;
	else tx_packet3[7] <= tx_packet3[7];
     end
     else begin
	tx_packet3[0] <= tx_packet3[0];
	tx_packet3[1] <= tx_packet3[1];
	tx_packet3[2] <= tx_packet3[2];
	tx_packet3[3] <= tx_packet3[3];
	tx_packet3[4] <= tx_packet3[4];
	tx_packet3[5] <= tx_packet3[5];
	tx_packet3[6] <= tx_packet3[6];
	tx_packet3[7] <= tx_packet3[7];
     end
   
   //----------------------------------------------------------------------
   // Do elink clk, mosi, and usr. (usr is just a scope trigger, for now)
   //----------------------------------------------------------------------

   always @(posedge clk80)
     if ((shift_counter == 8'h0) && (send_packet3 == 1'b1)) begin
	sent0 <= 1'b0;
	sent1 <= 1'b0;
	sent2 <= 1'b0;
	sent3 <= 1'b1;
	shift_counter <= bits_packet3[7:0];
	shift_mosi<={tx_packet3[7],tx_packet3[6],tx_packet3[5],tx_packet3[4],
		     tx_packet3[3],tx_packet3[2],tx_packet3[1],tx_packet3[0]};
     end
     else if ((shift_counter == 8'h0) && (send_packet2 == 1'b1)) begin
	sent0 <= 1'b0;
	sent1 <= 1'b0;
	sent2 <= 1'b1;
	sent3 <= 1'b0;
	shift_counter <= bits_packet2[7:0];
	shift_mosi<={tx_packet2[7],tx_packet2[6],tx_packet2[5],tx_packet2[4],
		     tx_packet2[3],tx_packet2[2],tx_packet2[1],tx_packet2[0]};
     end
     else if ((shift_counter == 8'h0) && (send_packet1 == 1'b1)) begin
	sent0 <= 1'b0;
	sent1 <= 1'b1;
	sent2 <= 1'b0;
	sent3 <= 1'b0;
	shift_counter <= bits_packet1[7:0];
	shift_mosi<={tx_packet1[7],tx_packet1[6],tx_packet1[5],tx_packet1[4],
		     tx_packet1[3],tx_packet1[2],tx_packet1[1],tx_packet1[0]};
     end
     else if (shift_counter == 8'h0) begin
	sent0 <= 1'b1;
	sent1 <= 1'b0;
	sent2 <= 1'b0;
	sent3 <= 1'b0;
	shift_counter <= bits_packet0[7:0];
	shift_mosi<={tx_packet0[7],tx_packet0[6],tx_packet0[5],tx_packet0[4],
		     tx_packet0[3],tx_packet0[2],tx_packet0[1],tx_packet0[0]};
     end
     else begin
	sent0 <= 1'b0;
	sent1 <= 1'b0;
	sent2 <= 1'b0;
	sent3 <= 1'b0;
	shift_counter <= shift_counter - elink_enable;
	shift_mosi <= (elink_enable == 1'b1) ? {1'b0, shift_mosi[127:1]} : 128'h0;
//	shift_mosi <= {1'b0, shift_mosi[127:1]};
//	shift_mosi <= {flip, shift_mosi[127:1]};
     end
   
//     reg flip = 1'b0;
//     always @(posedge clk80)
//     begin
//         flip <= ~flip;
//         elink_enable <= 1'b1;
//     end
//   always @(posedge clk80)
//    shift_mosi <= {flip, shift_mosi[127:1]} ;
     
   //----------------------------------------------------------------------
   // Do elink miso.
   // (use two different 160MHz delays to choose from during debug)
   //----------------------------------------------------------------------

   reg enable_miso0, enable_miso1, enable_miso2, enable_miso3;
      
   assign rx_packet0[0] = shift_miso0[15:0];
      assign rx_packet0[1] = shift_miso0[31:16];
      assign rx_packet0[2] = shift_miso0[47:32];
      assign rx_packet0[3] = shift_miso0[63:48];
      assign rx_packet0[4] = shift_miso0[79:64];
      assign rx_packet0[5] = shift_miso0[95:80];
      assign rx_packet0[6] = shift_miso0[111:96];
      assign rx_packet0[7] = shift_miso0[127:112];
   
      assign rx_packet1[0] = shift_miso1[15:0];
      assign rx_packet1[1] = shift_miso1[31:16];
      assign rx_packet1[2] = shift_miso1[47:32];
      assign rx_packet1[3] = shift_miso1[63:48];
      assign rx_packet1[4] = shift_miso1[79:64];
      assign rx_packet1[5] = shift_miso1[95:80];
      assign rx_packet1[6] = shift_miso1[111:96];
      assign rx_packet1[7] = shift_miso1[127:112];
   
      assign rx_packet2[0] = shift_miso2[15:0];
      assign rx_packet2[1] = shift_miso2[31:16];
      assign rx_packet2[2] = shift_miso2[47:32];
      assign rx_packet2[3] = shift_miso2[63:48];
      assign rx_packet2[4] = shift_miso2[79:64];
      assign rx_packet2[5] = shift_miso2[95:80];
      assign rx_packet2[6] = shift_miso2[111:96];
      assign rx_packet2[7] = shift_miso2[127:112];
   
      assign rx_packet3[0] = shift_miso3[15:0];
      assign rx_packet3[1] = shift_miso3[31:16];
      assign rx_packet3[2] = shift_miso3[47:32];
      assign rx_packet3[3] = shift_miso3[63:48];
      assign rx_packet3[4] = shift_miso3[79:64];
      assign rx_packet3[5] = shift_miso3[95:80];
      assign rx_packet3[6] = shift_miso3[111:96];
      assign rx_packet3[7] = shift_miso3[127:112];
   
   always @(posedge clk80) begin
      if (reset == 1'b1) enable_miso0 <= 1'b1;
      else if (shift_miso0[8:1] == FRAME_CMD) enable_miso0 <= 1'b0; // capture intial packet from sca
      else enable_miso1 <= enable_miso1;
   end
   
     

   always @(posedge clk80) begin
//      if ((reset == 1'b1) || (shift_miso1[8:1] == FRAME_CMD)) enable_miso1 <= 1'b0;
      //else enable_miso1 <= enable_miso1 | sent1;
       if ((reset == 1'b1) || ((shift_miso1[16:9] != IDLE_CMD) &&
     (shift_miso1[8:1] == IDLE_CMD))) enable_miso1 <= 1'b0;
      else enable_miso1 <= enable_miso1 | sent1 | sent2 | sent3; // for debug
   end
   
   always @(posedge clk80) begin
//      if ((reset == 1'b1) || (shift_miso2[8:1] == FRAME_CMD)) enable_miso2 <= 1'b0;
      //else enable_miso2 <= enable_miso2 | sent2;
       if ((reset == 1'b1) || ((shift_miso2[16:9] != IDLE_CMD) &&
     (shift_miso2[8:1] == IDLE_CMD))) enable_miso2 <= 1'b0;
      else enable_miso2 <= enable_miso2 | sent1 | sent2 | sent3; // for debug
   end

   always @(posedge clk80) begin
//      if ((reset == 1'b1) || (shift_miso3[8:1] == FRAME_CMD)) enable_miso3 <= 1'b0;
      //else enable_miso3 <= enable_miso3 | sent3;
       if ((reset == 1'b1) || ((shift_miso3[16:9] != IDLE_CMD) &&
     (shift_miso3[8:1] == IDLE_CMD))) enable_miso3 <= 1'b0;
      else enable_miso3 <= enable_miso3 | sent1 | sent2 | sent3; // for debug
   end

   // delay miso by a couple 160 MHz ticks
   reg dly1_miso, dly2_miso;
   always @(posedge clk160) dly1_miso = fmc_miso;
   always @(posedge clk160) dly2_miso = dly1_miso;
   
   // shift_miso0
   always @(posedge clk80) begin 
      if (reset == 1'b1) shift_miso0 <= 128'h0;
      else if (enable_miso0 == 1'b1)
       begin
	 shift_miso0[127] <= dly1_miso;
	 shift_miso0[126:0] <= shift_miso0[127:1];
      end
      else shift_miso0 <= shift_miso0;
   end
   
   // shift_miso1
   always @(posedge clk80) begin 
      if (reset == 1'b1) shift_miso1 <= 128'h0;
      else if (enable_miso1 == 1'b1) begin
	 shift_miso1[127] <= dly1_miso;
	 shift_miso1[126:0] <= shift_miso1[127:1];
      end
      else shift_miso1 <= shift_miso1;
   end
   
   // shift_miso2
   always @(posedge clk80) begin 
      if (reset == 1'b1) shift_miso2 <= 128'h0;
      else if (enable_miso2 == 1'b1) begin
	 shift_miso2[127] <= dly2_miso;
	 shift_miso2[126:0] <= shift_miso2[127:1];
      end
      else shift_miso2 <= shift_miso2;
   end
   
   // shift_miso3
   always @(posedge clk80) begin 
      if (reset == 1'b1) shift_miso3 <= 128'h0;
      else if (enable_miso3 == 1'b1) begin
	 shift_miso3[127] <= dly2_miso;
	 shift_miso3[126:0] <= shift_miso3[127:1];
      end
      else shift_miso3 <= shift_miso3;
   end




   //----------------------------------------------------------------------
   // Handle differential signals.
   //----------------------------------------------------------------------

   wire tmp_mosi, tmp_clk, tmp_usr, tmp_miso, null_miso;
   
   OBUFDS OBUFDS_mosi (.O(fmc_mosi_p), .OB(fmc_mosi_n), .I(tmp_mosi));
   OBUFDS OBUFDS_clk (.O(fmc_clk_p), .OB(fmc_clk_n), .I(tmp_clk));
   OBUFDS OBUFDS_usr (.O(fmc_usr_p), .OB(fmc_usr_n), .I(tmp_usr));
   IBUFDS IBUFDS_miso (.O(tmp_miso), .I(fmc_miso_p), .IB(fmc_miso_n));
   
   //----------------------------------------------------------------------
   // Use ODDR and IDDR registers for accurate ELINK edges.
   //----------------------------------------------------------------------

   reg dly_mosi, dly_usr;
   always @(posedge clk160) dly_mosi <= fmc_mosi;
   always @(posedge clk160) dly_usr <= fmc_usr;
   
   always @(posedge clk80)
     fmc_mosi <= shift_mosi[0];
   
   always @(posedge clk80)
     fmc_usr <= sent1 | sent2 | sent3;

   ODDR #(.DDR_CLK_EDGE("SAME_EDGE"), .INIT(1'b0), .SRTYPE("SYNC")) 
   ODDR_mosi 
     (.Q(tmp_mosi), .D1(dly_mosi), .D2(dly_mosi),
      .C(clk160), .CE(1'b1), .R(1'b0), .S(1'b0));

   ODDR #(.DDR_CLK_EDGE("SAME_EDGE"), .INIT(1'b0), .SRTYPE("SYNC")) 
   ODDR_clk 
     (.Q(tmp_clk), .D1(1'b0), .D2(1'b1),
      .C(clk40), .CE(1'b1), .R(1'b0), .S(1'b0));

   ODDR #(.DDR_CLK_EDGE("SAME_EDGE"), .INIT(1'b0), .SRTYPE("SYNC")) 
   ODDR_usr 
     (.Q(tmp_usr), .D1(dly_usr), .D2(dly_usr),
      .C(clk160), .CE(1'b1), .R(1'b0), .S(1'b0));
   
   IDDR #(.DDR_CLK_EDGE("OPPOSITE_EDGE"), 
	  .INIT_Q1(1'b0), .INIT_Q2(1'b0), .SRTYPE("SYNC"))
   IDDR_miso
     (.Q1(fmc_miso), .Q2(null_miso), 
      .D(tmp_miso),
      .C(clk160), .CE(1'b1), .R(reset), .S(1'b0));

endmodule